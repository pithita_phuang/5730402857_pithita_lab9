<?php
header("Content-type:text/xml");

$wsdl = 'http://webservicex.com/globalweather.asmx?wsdl';

$client = new SoapClient($wsdl);
$methodName = 'GetCitiesByCountry';
$params = array('CountryName'=>'Thailand');
$soapAction = "http://www.webserviceX.NET/GetCitiesByCountry";
$objectResult = $client->__soapCall($methodName, array('parameters' => $params), array('soapaction' => $soapAction));

$data = $objectResult->GetCitiesByCountryResult;
$xml = simplexml_load_string($data);

print($xml->asXML());


?>