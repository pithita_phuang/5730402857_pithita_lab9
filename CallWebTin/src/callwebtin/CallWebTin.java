/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package callwebtin;

import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.Name;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPFactory;
import javax.xml.soap.SOAPMessage;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;

/**
 *
 * @author pithitamacbook
 */
public class CallWebTin {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        CallWebTin ws = new  CallWebTin();
        ws.CallTinWs();
        // TODO code application logic here
    }
    
        public void CallTinWs() {
            
            try{
            
            SOAPConnection conn = SOAPConnectionFactory.newInstance().createConnection();
            //SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            //SOAPConnection conn = soapConnectionFactory.createConnection();
            MessageFactory mFac = MessageFactory.newInstance();
            SOAPMessage message = mFac.createMessage();
            //SOAPHeader header = message.getSOAPHeader();
            SOAPBody body = message.getSOAPBody();
            SOAPFactory factory = SOAPFactory.newInstance();
            String url = "https://rdws.rd.go.th/serviceRD3/checktinpinservice";
            String prefix = "ns";
            Name nameServiceTIN = factory.createName("ServiceTIN", prefix, url);
            Name nameUsername = factory.createName("username", prefix, url);
            Name namePassword = factory.createName("password", prefix, url);
            Name nameTIN = factory.createName("TIN", prefix, url);
            SOAPElement checkTINPIN = body.addBodyElement(nameServiceTIN);
            SOAPElement username = checkTINPIN.addChildElement(nameUsername);
            SOAPElement password = checkTINPIN.addChildElement(namePassword);
            SOAPElement tin = checkTINPIN.addChildElement(nameTIN);
            username.addTextNode("anonymous");
            password.addTextNode("anonymous");
            tin.addTextNode("1419900567361");
            MimeHeaders mimeHeader = message.getMimeHeaders();
            mimeHeader.addHeader("SOAPAction", "https://rdws.rd.go.th/serviceRD3/checktinpinservice/ServiceTIN");
            message.saveChanges();
            //Display Request Message
            System.out.println("REQUEST:");
            displayMessage(message);
            
            
            System.out.println("\n\n");
            //add code below for trust x.509 ceritficate
            XTrustProvider.install();
            SOAPMessage response = conn.call(message, "https://rdws.rd.go.th/serviceRD3/checktinpinservice.asmx");    
            System.out.println("RESPONSE:");
            //Display Response Message
            displayMessage(response);
          
        }
        catch (Exception e){
            
        }
        }
        
        public void displayMessage(SOAPMessage message) throws Exception {
        TransformerFactory tFact = TransformerFactory.newInstance();
        Transformer transformer = tFact.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");    
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        Source src = message.getSOAPPart().getContent();
        StreamResult result = new StreamResult(System.out);
        transformer.transform(src, result);
        }
}    

    

